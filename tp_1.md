# Part 1: Git alone

Git is a powerful tool to keep track of changes in your project and to manage your modifications history.
In this first part of the TP you will practice different git commands to keep track of your work and modifications with Git.

> A Git repository is an informatic project (folder) where changes are recorded using Git.

## Creation of a git repository

Start by creating a folder `alpha` in which you are going to write your project:

```sh
mkdir alpha # folder creation (makedirectory)
cd alpha # move into the folder (changedirectory)
```

Once in the alpha folder, you can initialize a Git repository.
A Git repository is a project tracked and indexed by git.

```sh
git init
ls -la # list directory
```

> The `git init` command create a hidden `.git` folder at the root of your project.

**You should not temper with the content of the `.git` folder.**
Everything in the `.git` folder belongs to Git the rest of the `alpha` folder belongs to you.

When you issue `git` command the content of the `.git` folder is accessed or modified by git.

## Using git to track changes

There is nothing in our repository:

```sh
git status
git st
```

We defined `st` as an alias of `status` in the `~/.gitconfig` file, so the two commands are equivalent.

Our first code:

```sh
mkdir data
git st
echo 'a' > data/letter.txt # display the letter "a" (echo) into (>) the file data/letter.txt
git st
```

**Git doesn’t track folders, only files**. For git folders are just structures to organise files.
With the creation of `letter.txt` git is aware of a change in the repository.
There are **untracked files**.

To start tracking files we use the command `git add` :

```sh
git add data/letter.txt
ls -R .git/objects/
git st
```

> `git add` greate **git blob**:
>
> - contains the compressed content of a file
> - The name of the blob is the [SHA1](https://en.wikipedia.org/wiki/SHA-1) of the file.
> - The first two characters of the [SHA1](https://en.wikipedia.org/wiki/SHA-1) are used as the name of a directory inside the objects folder
> - The rest of the hash is used as the name of the blob file

There are now changes to be committed, this means that git is now following the state of the `data/letter.txt` file.
The current state of `data/letter.txt` is recorded.

![git add made a copy of the file creating the corresponding blob](img/staging_1.png)

```sh
git ls-files --stage
echo "1234" > data/number.txt
git add data/number.txt
git ls-files --stage
echo "1" > data/number.txt
```

We changed the state of `data/number.txt`, but those changes are not staged to be committed.
The previous states of `data/number.txt` is still recorded *somewhere* even if it differs from its current state.

> This *somewhere* is called the **staging area** (where you stage changes).

```sh
git add data/number.txt
git ls-files --stage
```

![modifications in the working area don’t affect what’s stored in Git](img/staging_2.png)


![The staging area is different from the working directory](img/staging_3.png)

You can save the state of the staging area **definitively** with the command `git commit`

```sh
git commit -m "a1"
git st
```

Here "a1" is the message associated with the commit.

![Write clear and informative commit messages ([xkcd](https://xkcd.com/))](img/git_commit_xkcd.png)

> There are two rules for committing:
>
> - Commit often
> - Write clear and informative logs
> [http://git-scm.com/book/ch5-2.html](http://git-scm.com/book/ch5-2.html)

You cleared the changes to be committed and wrote them in a commit.

```sh
git log
git lo
```

You wrote your first commit with a unique identifier:
`531019e7119268c4dae5ac44ef5929165794f4b0` (not this exact identifier as you are not me and did not do the commit at the exact same time as me).

> `git commit`:
> - creates a tree graph to represent the content of the version of the project being committed
> - creates a commit object
> - points the current branch to the new commit object

```sh
git ls-tree -r 531019e7119268c4dae5ac44ef5929165794f4b0
git ls-tree -d 531019e7119268c4dae5ac44ef5929165794f4b0
```

The `531019e7119268c4dae5ac44ef5929165794f4b0` points to the tree object which points to the two committed files.

![The commit a1 record the tree structure and the files corresponding to the staging area](img/commit_a1.png)

In git, there are 3 areas:

- The **working area** (everything in `alpha/` except the folder `.git`)
- The **staging area** (in the `.git` folder)
- The **repository** (also in the `.git` folder)

![git areas](img/three_git_areas.png)

The repository is a chain of commit, beginning with your first commit.

### Navigating a git repository

Let’s create a new commit with the changes made to `data/number.txt`:

```sh
echo "2" > data/number.txt
git add data/number.txt
git commit -m "a2"
git st
git lo
```

You have a second point in your commit history.

You can navigate to a given point of the repository with the command `git checkout`:

```sh
git log
cat data/number.txt
git checkout 531019e 
cat data/number.txt
```

> You don't have to give the complete commit identifier for the command `git checkout`, only an unique subset of it.

The state, of the file `data/number.txt` is back to the one at the time of the first commit.
The `git checkout` or `git co` command rewrite your whole working area to match the state of the targeted commit.

```sh
git log
```

The `git log` command doesn’t display your second commit anymore.
Git commits only know of their direct ancestor(s).

The other change is in the first line of the `git log` command:
`(HEAD -> master)` became (HEAD).
This change means that you are in a **detached HEAD** state , the command `git st` also gives you this information.

```sh
git st
```

In git commit are chained one after another starting with the first commit.
Therefore, you can always go back to the commit history.

The repository is not only made of commits, but also of labels.
Labels point to a commit and can be the target of the `git co` command.
They are two types of labels:

- branches
- tags

If the first commit is the root of a growing tree of commits, branches are labels pointing to the leaves of the tree.
They change after each commit from pointing to the ancestor of the new commit, to pointing to the new commit.
The default branch in git is called `master`
Contrary to branches tags are anchored to a fixed commit.
**HEAD** is a special label that indicates your position in the tree of commits.

You can add a tag to your first commit with the command `git tag`

```sh
git tag -a v0.1 -m "my version 0.1"
git tag
git lo
```

You now have one tag in your repository.
The commit `c7a4cb9` is pointed by the tag `v0.1`.

```sh
git co master
git st
git lo
```

We are back at the leaf of the git repository.

## Growing the repository tree

We only have one branch, the `master` branch in our repository.
This means that we only have one timeline in our history.
In git, you can have as many branches as you want, to test out things, for example.

The following command creates the branch `dev` and move us (**HEAD**) to it.
```sh
git co -b dev
git st
git lo
```

```sh
echo ’3’ > data/number.txt
git add data/number.txt
git commit -m "a3"
git lo
```

If we have two branches `dev` and `master`, it’s hard to tell them apart (`master` is shorter).

```sh
git co master
echo 'b' > data/letter.txt
git add data/letter.txt
git commit -m "b2"
git lo
```

Congratulations, you have a fork in your git repository !

Let’s make another one called `dev2` from the branch `master` where `data/letter.txt` contain the letter `c`.
Then create the corresponding commit `c2`

## Merging branches

When you are ready to incorporate your new code in your main branch you need to merge the branch containing the new code into your main branch.
You can merge branches with the command `git merge`.
This command is going to try to merge the targeted branch into the branch you are on (**HEAD**).

The command `git branch` shows you the available branches and the one you are on.

```sh
git branch
git merge master
```

Can we merge `master` into `dev2` ?

```sh
git co master
git branch
git merge dev2 
```

There are three types of merge :
- Fast-forward
- Merge of two different lineages
- Merge of two different lineages with conflict

Here the branch `dev2` is a direct descendant of `master`.
`dev2` contain all the information contained in `master`.
For `master` to gain all the information contained in `dev2`, we just have to move the label `master` to the commit pointed by `dev2`

```sh
git lo
```

There is a fork between the branch `master` and `dev`, we have to merge two different lineages.

```sh
git branch
git merge dev
```

Here the command create a **merge commit**, a merge commit is a commit that has two direct ancestors.
A new commit means a new commit message to write.

```sh
git lo
```

The new commit is a `Merge` commit with the identifiers of its two ancestors.
Here, we didn't have any conflict: in the `dev` branch, we worked on the `data/number.txt` file while in the `master` branch, we worked on the `data/letter.txt` file.

Let’s complicate things !

Create a new commit `e3` in the `master` branch where the content of `data/letter.txt` is set to `e`.
Then create a new commit in the `dev` branch where the content of `data/letter.txt` is set to `f` and go back the branch `master`.

```sh
git lt
git branch
git merge dev
git st
```

The automatic merge failed due to a conflict in `data/letter.txt`.
You are now in a merging state.
You must solve the conflict by editing `data/letter.txt`.

You can see three things in the `data/letter.txt`:

- The state of the file in your current branch (**HEAD**)
- The state of the file in the last common ancestor of the two branches
- The state of the file in the merged branch (*dev*)

Edit `data/letter.txt`, to set its content to `f`.
Then complete the merge:

```sh
git add data/letter.txt
git st
git commit
git st
git lt
```

You, now, know how to deal with the different kind of merge

[ ****Next: Git remote**** ](./tp_2.md#part-2-git-remote)
