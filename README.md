# Git : the basis

## Practial guide

The practical guide is decomposed in three sections, after [configuring your ssh client](./tp.md):

- In the first part [ **Git Alone** ](./tp_1.md#part-1-git-alone), you will learn to use the basic Git command.
- In the second part [ **Git remote** ](./tp_2.md#part-2-git-remote), you will learn to interact with a Git remote repository
- In the last part [ **Git together** ](./tp_3.md#part-3-git-together), you will learn to work in collaboration with another person using Git.

## [Presentation support](./presentation.md)

The [presentation support](./presentation.md), provide advanced details on the mechanisms behind Git.
It can be read independently from the [partial guide](./tp.md).
