# Setup your environment

We are going to write some basic configuration files to be able to connect to the [gitlab server of the LBMC](http://gitbio.ens-lyon.fr).
If you are not using your own laptop, remember to delete your `~/.ssh` folder at the end of the TP.
Otherwise, somebody else could use your ssh key if you don’t protect them with a password.
But don’t stress too much about it, you can always remove the key later from your [gitlab configuration](http://gitbio.ens-lyon.fr/profile/keys).

Throughout this practical, every string between chevrons `<like this one>` must be replaced with the adequate value.

## Git configuration

Git can be configured with the `git config` command or directly by editing the `~/.gitconfig` file. Here is a config example to start:

```sh
# This is Git per-user configuration file.
[user]
name = <your name>
email = <your mail>

[core]
editor = vim

[alias]
co = checkout
ci = commit
cm = "commit -m"
st = status
lo = log --graph --decorate --date-order --all
lg = "log --pretty=format:\"%h - %an : %s\""
lt = log --graph --oneline --all

unstage = "reset HEAD"
diffs = "diff --stat"
diffh = "diff --staged"
diffc = "diff --check"
logc = "log -G"
	d = difftool
```

You can replace `vim` by any other editor of your choice, like `nano` (easier to learn) or `gedit` (graphical).

The **alias** section of the configuration is for shortcuts of git commands.
`git co` is now equivalent to `git checkout` but minimize the number of keys pressed.

## Gitlab connection

There are many ways to connect to a gitlab server (you are browsing one right now).
The git software relies on other system protocols to handle authentication and rights.

- To connect to the gitlab server via ssh, you first need to generate a ssh key:

```sh
ssh-keygen -t rsa -C "<your_email>@ens-lyon.fr" -b 4096 -f ~/.ssh/id_rsa_gitlab_lbmc
```

When you are on a UNIX system and when you have the right to remove the writing permission to your `~/.ssh/config` for the group and the others, you can use this method.
Otherwise, `ssh` won’t let you use this configuration file for security reasons.

- You need to add the following lines to the file `~/.ssh/config` to use it automatically for connection to `gitlab_lbmc`.

```sh
Host gitbio.ens-lyon.fr 
    HostName        gitbio.ens-lyon.fr
    User            git
    IdentitiesOnly  yes
    IdentityFile    ~/.ssh/id_rsa_gitlab_lbmc
```

This configuration provides you with the shortcut `gitlab_lbmc` to connect to the `gitlab.biologie.ens-lyon.fr` server on the port `2222` under the `git` user, without using a password and using the identity key ` ~/.ssh/id_rsa_gitlab_lbmc`.

- Finally, go to [https://gitbio.ens-lyon.fr/-/profile/keys](https://gitbio.ens-lyon.fr/-/profile/keys) and paste the content of the file `~/.ssh/id_rsa_gitlab_lbmc.pub`

To test your connection run:

```sh
ssh -T gitbio.ens-lyon.fr
```

**With this second method, you will have to replace every `https://gitlab.biologie.ens-lyon.fr/` url in the sequel with `gitlab_lbmc:`, the shortcut you defined in your `~/.ssh/config` file.**

[ **Next: Git Alone** ](./tp_1.md#part-1-git-alone)


