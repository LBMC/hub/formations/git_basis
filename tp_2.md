# Part 2: Git remote

We start by cloning an existing repository.
`file_handle.py` is a small python script to handle the dating and the access to dated files in a format compatible with the [guide of good practices at the LBMC](http://www.ens-lyon.fr/LBMC/intranet/fichiers/bioinfo/good-practices.pdf).
The `git clone <url>` command retrieves the whole history of a project and setup a working area based on the last point in this history.

```sh
cd ~/
git clone git@gitbio.ens-lyon.fr:LBMC/hub/file_handle.git git_basis_tp1
cd git_basis_tp1
ls -l
```

We can check the log (history) of this repository with the command `git log`
this command has a huge number of formatting options.

```sh
git log --graph --all
git lt
```

Git history is not necessarily linear. The bifurcations or branches are a useful
tool to try and implement new things without tempering with the main (`master`)
branch of a project.

To check the different branches of this project:

```sh
git branch
```

By default the main branch and the first branch of a git repository is called `master`.
In the framework of this TP we want to start working from a specific point of the
`file_handle` project history. The sha1 of this commit is `727b007b`.

```sh
git checkout 727b007b
```

We are now in a `detached HEAD state`. This means that the working area was set
to match the commit `727b007b` which is not pointed by any references.

We are in part 1 or this TP so we want to setup a new branch to work on.
The command `git checkout -b <branch>` creates the branch `<branch>` and update
the working directory to match it.

```sh
git checkout -b tp1
ls -l
```

The content of the branch `tp1` is the same as the one of the commit `727b007b`.

## Modification to Git history

Gitlab is able to render the [markdown format](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) of files with the `.md` extension.
To enable this rendering for the `README` file, we need to rename it. To do this we have
two options:

- Deleting the file and creating a new one with the commands `git rm README` and `git add README.md`.
- Directly renaming the file with the command `git mv README README.md`.

```sh
git mv README README.md
```

Why did we choose `git mv` instead of the other option (besides entering less git commands)?

The commands like `git mv` and `git rm` add the modification directly to the staging
area.

```sh
git status # README.md is in the staging area
git commit -m "README: rename to README.md for md rendering"
git status # README.md is in the git repository
```

The first thing is to add a title to this `README.md` file.

```sh
echo "# file_handle" > README.md # we add the name of the project in the README
git status # README.md is in the working area
git add README.md
git status # README.md is in the staging area
git commit -m "README.md: add title"
git status # README.md is in the git repository
```

![git lifecycle](img/git_lifecycle.png)

We then add a small description.

```sh
echo "smalll python software to handle dated file names" >> README.md
git status
git diff README.md
git add README.md
git commit -m "README: smalll description of the software"
```

We made a mistake in the last commit: there is a typo in the commit message and
we also wanted to delete the file `todo.txt`. The command `git commit --amend`
allows us to edit the last commit.

```sh
git rm todo.txt
git status
git commit --amend
```


## Setting up your git repository

We made some changes to the local git repository. We want to make them available
on the gitlab server. The command `git push <repos>` send your latest
modifications to the remote repository defined as `<repos>`. The default remote
repository is called `origin`

```sh
git push
```

You don’t have permission to write to this repository (if you want to contribute to the file_handle project you can ask me later).
To cope with this problem you are going to create your own remote repository for this project on the gitlab server.

To do so, go to [http://gitbio.ens-lyon.fr/](http://gitbio.ens-lyon.fr/) and click on **new project**.
You can give a name to your project (we are going to use `git_basis` in the following commands). Click on **Create project**.
You end up on your project home page.

The main information is in the **ssh** box containing a link to your
repository :

`git@gitbio.ens-lyon.fr:<user_name>/git_basis.git`.

with the https protocol we have:
`https://gitbio.ens-lyon.fr/<user_name>/git_basis.git`

> Note: repositories created on gitbio are _bare_ repository. There is no working
> or stash area on gitbio because nobody works on it.

At the end of the page [http://gitbio.ens-lyon.fr/<user_name>/git_basis](http://gitbio.ens-lyon.fr/<user_name>/git_basis) you have the following instruction to populate your remote repository
from an existing repository.

We can check the current remote addresses of our local git repository:

```sh
git remote -v
```

**origin** is set to `git@gitbio.ens-lyon.fr:LBMC/file_handle.git` for `fetch` and `push`
operation.

We want to add our repository to the remote addresses of our local git repository.
We are going to use `git@gitbio.ens-lyon.fr` url to match the ssh configuration file and use our ssh key in the connections to
this server.

```sh
git remote add perso git@gitbio.ens-lyon.fr:<user_name>/git_basis.git
git remote -v
```

Then we use the following commands to push our local copy to our remote repository:

```sh
git push -u perso --all
git push -u perso --tags # tags need to be pushed separatly
```

## merging branches

We want to merge our work with the **master** branch. What will the following
command do?

```sh
git merge master
```

Nothing. **master** points to an ancestor commit of the **tp1** branch.
However, the reverse operation is possible:

```sh
git checkout master
git branch -v
git merge tp1
git push perso
```

You can see the graph of our modifications at the following address [http://gitbio.ens-lyon.fr/<user_name>/git_basis/network/master](http://gitbio.ens-lyon.fr/<user_name>/git_basis/network/master)

[ ****Next: Git together**** ](./tp_3.md#part-3-git-together)
