# Part 3: Git together

Branch management is at the heart of Git.
These powerful mechanisms will help you to work in collaboration with others.
For the last part of the TP you need to pair with someone else. We will refer to you as developer **W** and the other person as developer **C**.

## Developer **W**:

For **C** to be able to push to you repository, you need to grant him access.
The gitbio access control is repository or branch-based.
Go to the project **Settings**, tab and then to **Member**.
You can search **C** and give him a role. Here we will use the role **Master** so **C** has as many rights as **W**.
Click on **Add to project** to validate the changes.

The idea behind a branch-based permission is to allow developers to work on a given branch of your repository while keeping others under your control.

## Developer **C**:

Clone **W**’s repository:

```sh
git clone git@gitbio.ens-lyon.fr:<W_name>/git_basis.git git_basis_tp2
cd git_basis_tp2
git remote -v
git checkout -b tp2
```

The only remote that **C**’s local repository knows is **W**’s remote.

**C** adds a dependency section to `README.md`

````md
## Dependencies
The only dependencies for file_handle.py are `python3` and the `argspaste`

```sh
sudo install python3 pip3
pip3 install argparse
```
````

```sh
git add README.md
git commit -m "README.md: add dependencies description"
git status
```

Developer **C** is now ahead of `remote/master` by one commit.

```sh
git push
```

The command is not working.
Git writes a useful message giving you the right command for this first push.

## Developer **W**:

Before starting to work on the `file_handle`, developer **W** want to get the last version of the project.

```sh
git branch
git fetch perso
git status
git branch
```

**W** sees that a new `tp2` branch was created.

```sh
git checkout tp2
git branch
```

**W** wants to add a small help section to the `README.md`.

````md
## Usage
To know how to use the `file_handle.py` script, enter the following command:

```sh
file_handle.py --help
```
````

```sh
git add README.md
git commit -m "README.md: add Usage section"
git status
```

## Developer **C**:

While **W** is contributing to the redaction of the documentation, developer **C** continue to work on it:

```sh
git fetch
git status
```

````sh
## Usage
To use the `file_handle.py` script, enter the following command:

```sh
file_handle.py -f <INPUT_FILE>
```

if `<INPUT_FILE>` exists and is not dated `file_handle.py` will date it.
Then `file_handle.py` will return the absolute path to the last version of
`<INPUT_FILE>`.
````

```sh
git add README.md
git commit -m "README.md: add -f option description"
git status
git push
````

## Developer **W**:

```sh
git push
```

When **W** tries to push his modifications to the remote repository he has an error because **C** made changes to it.
**W** has to get these modifications.
Git advises you to use the command `git pull`. We are not going to use this command and instead to:

```sh
git fetch
git status
git merge FETCH_HEAD
```

Those two commands do the same jobs as the command `git pull`, with the exception that we get to check what are the changes before blindly merge them.

There seems to be a conflict.
The command `git status` gives us the steps to solve this conflict.

In the working ares, the `README.md` file contains the information of this conflict.

````diff
<<<<<<< HEAD
To know how to use the `file_handle.py` script, enter the following command:

```sh
file_handle.py --help
```
=======
To use the `file_handle.py` script, enter the following command: ```sh
file_handle.py -f <INPUT_FILE>
```
if `<INPUT_FILE>` exists and is not dated `file_handle.py` will date it. Then `file_handle.py` will return the absolute path to the last version of `<INPUT_FILE>`.
>>>>>>> c470f055f499bc13fa64d90f3b5cb38314b34b6e
````

The section **HEAD** corresponds to the version of the **Usage** section in your local repository at the position of the **READ** reference.
The second section corresponds to the version of the **Usage** section in the remote repository for the commit **c470f055f499bc13fa64d90f3b5cb38314b34b6e**, the last commit of **C**.

To resolve it you need to edit it and then run the following commands to complete the merge:

```sh
git add README.md
git commit
git push
```

# Conclusion

You have performed a lot of basic and reasonably advanced git operations in a scenario with a central git repository.  
To go further, you can study a few things that we did not cover here:

- Moving or removing files
- Stashing
- Tagging
- Cherry-Picking
- Rewriting history (interactive rebasing)
- Bisecting
