all: presentation.pdf

presentation.pdf: presentation.tex header.tex
	lualatex -shell-escape presentation.tex

clean:
	rm presentation.{aux,bcf,dvi,log,nav,out,run.xml,snm,toc,pdf}
